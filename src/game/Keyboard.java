package game;

import utils.Res;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // don't let move the castle and the arrow
                if (Main.scene.getXPos() == -1) {
                    Main.scene.setXPos(0);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX(750);
                }
                Main.scene.getMario().setMoving(true);
                Main.scene.getMario().setToRight(true);
                Main.scene.setxSpeed(Main.scene.getMario().getXSpeed());
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getXPos() == 4601) {
                    Main.scene.setXPos(4600);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX(750);
                }

                Main.scene.getMario().setMoving(true);
                Main.scene.getMario().setToRight(false);
                Main.scene.setxSpeed(Main.scene.getMario().getXSpeed());
            }
            // jump
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (!Main.scene.getMario().isJumping()){
                    Main.scene.getMario().setJumping(true);
                    Audio.playSound(Res.AUDIO_JUMP);
                }
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.getMario().setMoving(false);
        Main.scene.setxSpeed(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
