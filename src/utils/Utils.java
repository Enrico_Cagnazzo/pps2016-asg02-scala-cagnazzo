package utils;

import javax.swing.*;
import java.awt.*;
import java.io.PrintStream;
import java.net.URL;

/**
 * @author Roberto Casadei
 */

public class Utils {
    private static URL getResource(String path){
        return Utils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }

    public static void printLog(PrintStream err, String message) {
        err.println(message);
    }
}
