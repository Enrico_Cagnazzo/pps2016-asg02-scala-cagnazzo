package gameComponent

/**
  * Created by Enrico on 11/03/2017
  */
trait GameComponent {
    def getWidth: Int

    def getHeight: Int

    def getX: Int

    def getY: Int
}