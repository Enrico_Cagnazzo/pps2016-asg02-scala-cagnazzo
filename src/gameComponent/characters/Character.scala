package gameComponent.characters

import gameComponent.GameComponent

trait Character extends GameComponent {
    def walkImageURL(name: String, frequency: Int): String

    def deadImageURL(name: String): String

    def isNearby(gameComponent: GameComponent): Boolean

    def hitAhead(gameComponent: GameComponent): Boolean

    def hitBack(gameComponent: GameComponent): Boolean

    def hitBelow(gameComponent: GameComponent): Boolean

    def hitAbove(gameComponent: GameComponent): Boolean

    def contact(gameComponent: GameComponent): Unit

    def move(sceneXSpeed:Int): Unit

    def isAlive: Boolean

    def die(): Unit
}