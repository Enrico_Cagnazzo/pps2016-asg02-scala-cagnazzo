package gameComponent.characters

/**
  * Created by Enrico on 09/04/2017.
  */
trait StaticEnemy extends Enemy{
    def getImageURL(name: String): String
}
