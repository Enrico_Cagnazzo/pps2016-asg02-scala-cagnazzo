package gameComponent.characters

object Turtle {
    private val WIDTH = 43
    private val HEIGHT_ALIVE = 50
}

class Turtle(x: Int,y: Int) extends BasicCharacter(x, y, Turtle.WIDTH, Turtle.HEIGHT_ALIVE) with Runnable with Enemy {
    super.setToRight(true)
    super.setMoving(true)
    val turtleThread = new Thread(this)
    turtleThread.start()

}