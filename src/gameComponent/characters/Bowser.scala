package gameComponent.characters

import utils.Res


object Bowser {
    private val WIDTH = 118
    private val HEIGHT_ALIVE = 170
}

class Bowser(x: Int,y: Int) extends BasicCharacter(x, y, Bowser.WIDTH, Bowser.HEIGHT_ALIVE) with Runnable with StaticEnemy {
    super.setToRight(true)
    super.setMoving(true)
    val bowserThread = new Thread(this)
    bowserThread.start()

    override def getImageURL(name: String): String = Res.IMG_BASE + name + Res.IMG_EXT
}