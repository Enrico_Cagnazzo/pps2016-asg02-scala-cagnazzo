package gameComponent.characters

/**
  * Created by Enrico on 12/03/2017.
  */
trait CharactersFactory {
    def createMario(): Mario

    def createAMushroom(x: Int, y: Int): Mushroom

    def createATurtle(x: Int, y: Int): Turtle

    def createBowser(x:Int, y:Int): Bowser
}

class CharactersConcreteFactory extends CharactersFactory {
    override def createMario() = new Mario()

    override def createAMushroom(x: Int, y: Int) = new Mushroom(x, y)

    override def createATurtle(x: Int, y: Int) = new Turtle(x, y)

    override def createBowser(x:Int, y:Int)= new Bowser(x,y)
}