package gameComponent.characters

import game.Audio
import game.Main
import gameComponent.GameComponent
import gameComponent.objects.Money
import utils.Res

object Mario {
    private val FLOOR_OFFSET_Y_INITIAL = 293
    private val X_START=300
    private val Y_START=243
    private val WIDTH = 28
    private val HEIGHT = 50
    private val JUMPING_LIMIT = 42
    private val FALL_VERTICAL_SPEED = 1
    private val JUMP_VERTICAL_SPEED = 4
    private val X_SPEED : Int = 1
}


class Mario() extends BasicCharacter(Mario.X_START, Mario.Y_START, Mario.WIDTH, Mario.HEIGHT) {

    private var jumping = false
    private var won=false
    private var jumpingExtent = 0
    private var diedAudio : Audio = _
    private var winAudio : Audio = _

    def isJumping: Boolean = jumping
    def setJumping(jumping: Boolean): Unit = this.jumping = jumping

    def hasWon: Boolean=won

    def getXSpeed: Int =
        if (!isMoving) 0
        else if (isToRight) Mario.X_SPEED
        else -Mario.X_SPEED

    def doJump(): String = {
        var imageName: String = null
        jumpingExtent += 1
        if (jumpingExtent < Mario.JUMPING_LIMIT) {
            if (getY > Main.scene.getHeightLimit) setY(getY - Mario.JUMP_VERTICAL_SPEED)
            else jumpingExtent = Mario.JUMPING_LIMIT
            imageName = if (isToRight) Res.IMG_MARIO_SUPER_DX
                        else Res.IMG_MARIO_SUPER_SX
        }
        else if (getY + getHeight < Main.scene.getFloorOffsetY) {
            setY(getY + Mario.FALL_VERTICAL_SPEED)
            imageName = if (isToRight) Res.IMG_MARIO_SUPER_DX
            else Res.IMG_MARIO_SUPER_SX
        }
        else {
            imageName = if (isToRight) Res.IMG_MARIO_ACTIVE_DX
            else Res.IMG_MARIO_ACTIVE_SX
            jumping = false
            jumpingExtent = 0
        }
        imageName
    }

    private def letFall() = {
        jumping = true
        jumpingExtent = Mario.JUMPING_LIMIT
    }

    override def contact(`object`: GameComponent): Unit = `object` match {
        case bowser:Bowser=>
            if (hitAbove(bowser) || hitAhead(bowser) || hitBack(bowser) || hitBelow(bowser))
                die()
        case enemy: Enemy=>
            if (enemy.isAlive) {
                if (hitAhead(enemy) || hitBack(enemy))
                    die()
                else if (hitBelow(enemy))
                    enemy.die()
            }
        case _ =>
            if (hitAhead(`object`) && isToRight || hitBack(`object`) && !isToRight) {
                Main.scene.setxSpeed(0)
                setMoving(false)
            }
            if (hitBelow(`object`) && jumping) Main.scene.setFloorOffsetY(`object`.getY)
            else if (!hitBelow(`object`)) {
                Main.scene.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL)
                if (hitAbove(`object`)) Main.scene.setHeightLimit(`object`.getY + `object`.getHeight) // the new sky goes below the object
                else if (!hitAbove(`object`) && !jumping) Main.scene.setHeightLimit(0) // initial sky
                if (!jumping && y < Mario.Y_START) letFall()
            }
    }


    def contactPiece(money: Money): Boolean = hitBack(money) || hitAbove(money) || hitAhead(money) || hitBelow(money)

    override def move(sceneXSpeed: Int): Unit = if (Main.scene.getXPos >= 0) setX(getX - sceneXSpeed)

    override def die(): Unit = {
        super.die()
        if (diedAudio == null) {
            diedAudio = new Audio(Res.AUDIO_DIED)
            diedAudio.playLoop()
        }
    }

    def win():Unit={
        if (winAudio==null){
            winAudio=new Audio(Res.AUDIO_WIN)
            winAudio.playLoop()
        }
        won=true
    }

}