package gameComponent.characters

object Mushroom {
    private val WIDTH = 27
    private val HEIGHT_ALIVE = 30
}

class Mushroom(x: Int, y: Int) extends BasicCharacter(x, y, Mushroom.WIDTH, Mushroom.HEIGHT_ALIVE) with Runnable with Enemy {
    setToRight(true)
    setMoving(true)
    val mushroomThread = new Thread(this)
    mushroomThread.start()
}