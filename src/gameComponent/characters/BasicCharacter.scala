package gameComponent.characters

import gameComponent.GameComponent
import utils.Res

object BasicCharacter {
    private val PROXIMITY_MARGIN = 0
    private val OBJECT_OFFSET = 5
    private val SPEED_X = 1
}

class BasicCharacter protected(var x: Int, var y: Int, val width: Int, val height: Int) extends Runnable with Character {
    final private val PAUSE = 15
    private var moving = false
    private var toRight = true
    private var counter = 0
    private var alive = true
    private var showFirstFrame = true

    override def getX: Int = x

    override def getY: Int = y

    override def getWidth: Int = width

    override def getHeight: Int = height

    override def isAlive: Boolean = alive

    def isToRight: Boolean = toRight

    def isMoving: Boolean = moving

    def setX(x: Int): Unit = this.x = x

    def setY(y: Int): Unit = this.y = y

    def setMoving(moving: Boolean): Unit = this.moving = moving

    def setToRight(toRight: Boolean): Unit = this.toRight = toRight

    def imageURL(name: String, status: String): String = {
        var string=Res.IMG_BASE + name + status
        string= string concat(if (this.toRight)  Res.IMAGE_PREFIX_DIRECTION_DX
                                    else Res.IMAGE_PREFIX_DIRECTION_SX
        ) concat Res.IMG_EXT

        string
    }

    override def walkImageURL(name: String, frequency: Int): String = {
        this match {
            case _=> {
                if ( {counter += 1;counter} % frequency == 0)
                    showFirstFrame = !showFirstFrame
                val status = if (!moving || !showFirstFrame) Res.IMAGE_PREFIX_STATUS_ACTIVE
                else Res.IMAGE_PREFIX_STATUS_NORMAL
                imageURL(name, status)
            }
        }

    }

    override def deadImageURL(name: String): String = imageURL(name, Res.IMAGE_PREFIX_STATUS_DEAD)

    override def move(sceneXSpeed:Int ): Unit = {
        this match {
            case _: Bowser=> this.x = this.x - sceneXSpeed
            case _ => this.x = this.x + (if (isAlive) { if (isToRight) BasicCharacter.SPEED_X  else -BasicCharacter.SPEED_X }else 0) - sceneXSpeed
        }

    }

    override def die(): Unit = {
        moving = false
        alive = false
    }

    private def sameXPosition(og: GameComponent) = this.x + this.width >= og.getX + BasicCharacter.OBJECT_OFFSET && this.x <= og.getX + og.getWidth - BasicCharacter.OBJECT_OFFSET

    private def sameYPosition(og: GameComponent) = this.y + this.height > og.getY && this.y < og.getY + og.getHeight

    override def hitAhead(og: GameComponent): Boolean = this.x + this.width >= og.getX && this.x + this.width <= og.getX + BasicCharacter.OBJECT_OFFSET && sameYPosition(og)

    override def hitBack (og: GameComponent): Boolean = this.x + this.width >= og.getX + og.getWidth - BasicCharacter.OBJECT_OFFSET && this.x <= og.getX + og.getWidth && sameYPosition(og)

    override def hitBelow(og: GameComponent): Boolean = sameXPosition(og) && this.y + this.height >= og.getY && this.y + this.height <= og.getY + BasicCharacter.OBJECT_OFFSET

    override def hitAbove(og: GameComponent): Boolean = sameXPosition(og) && this.y >= og.getY + og.getHeight && this.y <= og.getY + og.getHeight + BasicCharacter.OBJECT_OFFSET

    override def isNearby(component: GameComponent): Boolean = (this.x > component.getX - BasicCharacter.PROXIMITY_MARGIN && this.x < component.getX + component.getWidth + BasicCharacter.PROXIMITY_MARGIN) || (this.x + this.width > component.getX - BasicCharacter.PROXIMITY_MARGIN && this.x + this.width < component.getX + component.getWidth + BasicCharacter.PROXIMITY_MARGIN)

    override def contact(component: GameComponent): Unit = if (hitAhead(component) && isToRight) this.setToRight(false)
    else if (hitBack(component) && !isToRight) setToRight(true)

    override def run(): Unit =
        while (isAlive) {
            try Thread.sleep(PAUSE)
            catch {
                case e: InterruptedException =>
            }
        }
}