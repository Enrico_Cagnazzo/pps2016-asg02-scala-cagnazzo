package gameComponent.objects

import utils.Res
import utils.Utils

object Tunnel {
    private val WIDTH = 43
    private val HEIGHT = 65
}

class Tunnel(x: Int, y: Int) extends GameObject(x, y, Tunnel.WIDTH, Tunnel.HEIGHT) {
    super.setImageObject(Utils.getImage(Res.IMG_TUNNEL))
}