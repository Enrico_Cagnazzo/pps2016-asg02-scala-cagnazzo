package gameComponent.objects

import java.awt.Image
import game.Main
import gameComponent.GameComponent

class GameObject(var x: Int, var y: Int, var width: Int, var height: Int) extends GameComponent {
    private var imageObject: Image = null

    override def getWidth: Int = width

    override def getHeight: Int = height

    override def getX: Int = x

    override def getY: Int = y

    def getImageObject: Image = imageObject

    def setImageObject(imageObject: Image): Unit = this.imageObject = imageObject

    def move(sceneXSpeed: Int): Unit = if (Main.scene.getXPos >= 0) x = x - sceneXSpeed
}