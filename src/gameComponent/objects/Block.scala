package gameComponent.objects

import utils.Res
import utils.Utils

object Block {
    private val WIDTH = 30
    private val HEIGHT = 30
}

class Block(x: Int, y: Int) extends GameObject(x, y, Block.WIDTH, Block.HEIGHT) {
    super.setImageObject(Utils.getImage(Res.IMG_BLOCK))
}