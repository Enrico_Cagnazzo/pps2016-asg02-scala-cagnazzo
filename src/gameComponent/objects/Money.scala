package gameComponent.objects

import utils.Res
import utils.Utils
import java.awt.Image

object Money {
    private val WIDTH = 30
    private val HEIGHT = 30
    private val PAUSE = 10
    private val FLIP_FREQUENCY = 100
}

class Money(x: Int, y: Int) extends GameObject(x, y, Money.WIDTH, Money.HEIGHT) with Runnable {
    super.setImageObject(Utils.getImage(Res.IMG_PIECE1))
    private var counter = 0
    private var showFirstFrame = true

    def imageOnMovement: String = {
        if ( {
            this.counter += 1; this.counter
        } % Money.FLIP_FREQUENCY == 0) showFirstFrame = !showFirstFrame
        if (showFirstFrame) Res.IMG_PIECE1
        else Res.IMG_PIECE2
    }

    override def run(): Unit = while (true) {
        this.imageOnMovement
        try Thread.sleep(Money.PAUSE)
        catch {
            case e: InterruptedException =>
        }
    }
}